/**
*  \file Sortie.C
*  \author OUTTARA MOCTAR - KOUADIO BONI - AGNERO NOMEL
*  \version V.21
*  \brief Programme de gestion des Sorties
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <mysql/mysql.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>

/*
----------------------------------------------------------------------------------------
|             DECLARATION DES FONCTIONS EXECUTEES EN SORTIE DU PARKING                 |
----------------------------------------------------------------------------------------
*/


/**
* \fn char tempspasse()
* \brief Fonction de calcul du temps passée dans la parking des entrees
* \param conn
* \param idVehiculeSortie
* \details La fonction fait la difference entre le date de sortie t la date d'entree
*/
char *tps;
char *tempspasse(MYSQL *con, int idVehiculeSortie)
{
    char requete[200] = "";

    sprintf(requete, "SELECT CONVERT(TIMESTAMPDIFF(HOUR,date_entree,date_sortie), SIGNED INTEGER) FROM stationnement WHERE idVehicule=%d;", idVehiculeSortie);
    mysql_query(con, requete);

    MYSQL_RES *result = mysql_store_result(con);
    if (result == NULL)
    {
        fprintf(stderr, "%s\n", mysql_error(con));
        mysql_close(con);
        exit(1);
    }

    int num_fields = mysql_num_fields(result);
    MYSQL_ROW row;

    while ((row = mysql_fetch_row(result)) != 0)
    {
        for (int i = 0; i < num_fields; i++)
        {
            tps = row[0] ? row[0] : "NULL";
        }
    }
    mysql_free_result(result);
    printf("LE TEMPS PASSE EN STATIONNEMENT EST DE %s HEURES\n", tps);

    //int duree = 0;
    //duree = atoi(tps);

    //printf("duree : %d", duree);

    return tps;
}

/**
* \fn int prix()
* \brief Fonction de calcul du prix de stationnement dans la parking
* \details La fonction determine en fonction du type de vehicule
*/

//DECLARATION DE LA FONCTION PRIX
int prix(char type[200], int TPS)
{
    //TPS = atoi(tps);
    int prix = 0;

    if (strcmp(type, "CAMION")==0)
    {
        
        if (TPS<24 && TPS>0)
        {
             prix = TPS*700;
        } 
        else 
        {
            if(TPS == 0){
                prix = 700;
         }
         else {
             prix = 30000;
         }
        }
        
    }
    else{
        if (strcmp(type, "CLASSIQUE")==0)
    {
        
        printf("%d",TPS);
        if (TPS<24 && TPS>0)
        {
             prix = TPS*500;
        } 
        else 
        {
            if(TPS == 0){
             prix = 500;
         }
         else {
             prix = 20000;
         }
        }
        
    }
    else{
        if (strcmp(type, "2 ROUES")==0)
    {

        
        if (TPS<24 && TPS>0)
        {
             prix = TPS*300;
        } 
        else 
        {
            if(TPS == 0){
             prix = 300;
         }
         else {
             prix = 10000;
         }
        }
        
    }
    }
        
    }

    printf("Prix : %d\n", prix);
    

    return prix;
}

// Calcul du prix en fonction du temps passé dans le parking et du type de véhicule

/*
----------------------------------------------------------------------------------------
|          DECLARATION DE FONCTION PRINCIPALE EXECUTEE A LA SORTIE DU PARKING           |
----------------------------------------------------------------------------------------
*/

/**
* \fn void sortie()
* \brief Procedure de gestion des entrees des Vehicules.
* \details La fonction effectue la sortie des vehicules et affiche le nombre de places disponibles a la sortie!
*/

void sortie(MYSQL *conn, int *nbplacedispoVoitureClass, int *nbplacedispoCamion, int *nbplacedispo2roues)
{
    // Declaration de variables
    int typeVehicule;
    int idVehicule;
    int idVehiculeSortie;
    char typeStationnement[100];
    char temps[10];

    //Declaration variable de chargement de requete

    char requete2[200] = "";
    char requete_ajout[500] = "";

    //Parametre de MYSQL
    MYSQL_RES *result;
    MYSQL_RES *result2;
    MYSQL_ROW row;

    printf("------ Pour sortir du parking veuillez entrer les informations suivantes ---------\n");
    printf("identifiant du tiket : ");
    scanf("%d", &idVehiculeSortie);
    printf("%d", idVehiculeSortie);

    // Insertion de la date de sortie du vehicule de parking
    sprintf(requete_ajout, "UPDATE stationnement SET date_sortie=NOW() WHERE idVehicule='%d';", idVehiculeSortie);
    mysql_query(conn, requete_ajout);

    printf("\n\t====    insertion effectué avec succès   ====\t\n\n");

    //Requete de recuperation de la duree d'un vehicule dans le parking
    int TPS;
    TPS = atoi(tempspasse(conn, idVehiculeSortie));

    sprintf(requete2, "SELECT typeVehicule FROM stationnement WHERE idVehicule=%d;", idVehiculeSortie);
    mysql_query(conn, requete2);

    result2 = mysql_store_result(conn);
    if (result2 == NULL)
    {
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    int num_fields = mysql_num_fields(result2);
    MYSQL_ROW row_2;
    char *type;

    while ((row_2 = mysql_fetch_row(result2)) != 0)
    {
        type = row_2[0] ? row_2[0] : "NULL";

        printf("LE VEHICULE EST DE TYPE %s \n", type);
    }

    printf("VOUS DEVEZ PAYER %d FCFA\n", prix(type, TPS));

    if (type == "CLASSIQUE")
    {
        *nbplacedispoVoitureClass = *nbplacedispoVoitureClass + 1;
    }
    else
    {
        if (type == "CAMION")
        {
            *nbplacedispoCamion = *nbplacedispoCamion + 1;
        }
        else
        {
            *nbplacedispo2roues = *nbplacedispo2roues + 1;
        }

        printf("------------------- NOMBRE DE PLACES RESTANTES ----------------\n\n");

        printf("Les places disponibles dans notre parking sont: \n\n");
        printf("----------------------------------------------------------------\n\n");
        printf("|                 Vehicules classiques: %d                     |\n\n", *nbplacedispoVoitureClass);
        printf("----------------------------------------------------------------\n\n");
        printf("|                          Camions: %d                         | \n\n", *nbplacedispoCamion);
        printf("----------------------------------------------------------------\n\n");
        printf("|                     Engins à 2 roues: %d                     |\n\n", *nbplacedispo2roues);
        printf("----------------------------------------------------------------\n\n");
    }

    //printf("LE TEMPS PASSE EN STATIONNEMENT EST DE %s HEURES\n", tps);
    //return tps;

    //tempspasse(mysql_init(NULL),idVehiculeSortie);
}
