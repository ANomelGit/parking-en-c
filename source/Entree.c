/**
*  \file Entree.C
*  \author OUTTARA MOCTAR - KOUADIO BONI - AGNERO NOMEL
*  \version V.21
*  \brief Programme de gestion des Arrivees
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <mysql/mysql.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>


/*
----------------------------------------------------------------------------------------
|             DECLARATION DES FONCTIONS EXECUTEES A L'ENTREE DU PARKING                |
----------------------------------------------------------------------------------------
*/

/**
* \fn void entree()
* \brief Procedure de gestion des entrees des Vehicules.
* \details La fonction effectue les entrees des vehicules et affiche le nombre de places disponibles a la sortie !
*/


void entree(MYSQL *conn, int *nbplacedispoVoitureClass,int *nbplacedispoCamion, int *nbplacedispo2roues)
{
    // Declaration de variables
    _Bool permissionDeStationnemnt = true;
    int typeVehicule ;
    int idVehicule;
    char idVehiculeSortie[100];
    char typeStationnement[100];


printf("------------------- AFFICHAGE DE PLACES DISPONIBLE ----------------\n\n");

printf("Les places disponibles dans notre parking sont: \n\n");
printf("----------------------------------------------------------------\n\n");
printf("|                 Vehicules classiques: %d                     |\n\n", *nbplacedispoVoitureClass );
printf("----------------------------------------------------------------\n\n");
printf("|                          Camions: %d                         | \n\n", *nbplacedispoCamion);
printf("----------------------------------------------------------------\n\n");
printf("|                     Engins à 2 roues: %d                     |\n\n", *nbplacedispo2roues);
printf("----------------------------------------------------------------\n\n");

printf("Veuillez remplir les informations suivantes pour stationner \n\n");

printf("Entrer le type du vehicule (1-CLASSIQUE, 2-CAMION, 3- 2 ROUES) : \n");
scanf("%d", &typeVehicule); 
printf("\n");

          
switch (typeVehicule) {
    case 1:
        if (*nbplacedispoVoitureClass == 0) {
            if (nbplacedispoCamion > 0){
               typeVehicule = 2;
            }
            else {
                if(*nbplacedispo2roues > 0) {
                    typeVehicule = 3;
                }
                else{
                    printf("Plus de places disponibles \n");
                    permissionDeStationnemnt = false;
                }
            }
                    
        }
        break;
    case 2:
        if (*nbplacedispoCamion == 0) {
           if (*nbplacedispoVoitureClass > 0){
                typeVehicule = 1;
            }
            else {
                if(*nbplacedispo2roues > 0) {
                    typeVehicule=3;
                }
                else{
                    printf("Plus de places disponibles \n");
                    permissionDeStationnemnt = false;
                }
            }
                    
        }
        break;
    case 3:
        if (*nbplacedispo2roues == 0) {
            if (*nbplacedispoCamion > 0){
            typeVehicule = 2;
            }
            else {
                if(*nbplacedispoVoitureClass > 0) {
                    typeVehicule = 1;
                }
                else{
                    printf("Plus de places disponibles");
                    permissionDeStationnemnt = false;
                }
            }
                    
       }           
        break;
}

switch (typeVehicule) {
    case 1:
        strcpy(typeStationnement,"CLASSIQUE");
        break;
    case 2:
        strcpy(typeStationnement,"CAMION");
        break;
    case 3:
        strcpy(typeStationnement,"2 ROUES");         
        break;
  }

if (typeVehicule==1){
    *nbplacedispoVoitureClass= *nbplacedispoVoitureClass-1;
}
else{
    if(typeVehicule==2){
        *nbplacedispoCamion= *nbplacedispoCamion-1;
    }
    else{
        *nbplacedispo2roues= *nbplacedispo2roues-1;
    }
}

printf("%s \n",typeStationnement);


 if (permissionDeStationnemnt) {

    
    char requete_ajout[500]="";
    char requete_time [500]="";
    sprintf(requete_ajout, "INSERT INTO stationnement (date_entree, typeVehicule) VALUES(NOW(),'%s');", typeStationnement);

    mysql_query(conn, requete_ajout);
    
    
    printf("\n        ==== Stationnement effectué avec succès ==== \n");

printf("------------------- NOMBRE DE PLACES RESTANTES ----------------\n\n");

printf("Les places disponibles dans notre parking sont: \n\n");
printf("----------------------------------------------------------------\n\n");
printf("|                 Vehicules classiques: %d                     |\n\n", *nbplacedispoVoitureClass );
printf("----------------------------------------------------------------\n\n");
printf("|                          Camions: %d                         | \n\n", *nbplacedispoCamion);
printf("----------------------------------------------------------------\n\n");
printf("|                     Engins à 2 roues: %d                     |\n\n", *nbplacedispo2roues);
printf("----------------------------------------------------------------\n\n");
    
 }
}
