/**
*  \file main.C
*  \author OUTTARA MOCTAR - KOUADIO BONI - AGNERO NOMEL
*  \version V.21
*  \brief Programme principal
*
*/



#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <mysql/mysql.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "../Header/Entree.h"
#include "../Header/Sortie.h"
#include "../Header/Connexion.h"
#include <stdbool.h>


int main ()

{

MYSQL *conn = mysql_init(NULL);

connexion(conn); 
    
int choix;

int nbplacedispoVoitureClass =100;
int nbplacedispoCamion = 20;
int nbplacedispo2roues =30;

printf("----------------------------------------------------------------\n\n");
printf("                === BIENVENUE AU PARKING IVOIRE ===             \n\n");
printf("----------------------------------------------------------------\n\n");
printf("Notre parking est ouvert pour un stationnement securisé pour \nnotre clientèle... \n\n");

printf("Pour stationner, entrer '1' et pour sortir du parking, entrer '0'  \n\n");   
printf("Faites votre choix !!! \n"); 
 
scanf("%d",&choix);

while (choix== 0 || choix == 1) {

switch(choix){
    case 1 : entree(conn, &nbplacedispoVoitureClass, &nbplacedispoCamion, &nbplacedispo2roues); break;

    case 0 : sortie(conn, &nbplacedispoVoitureClass, &nbplacedispoCamion, &nbplacedispo2roues); break;
   
}
printf("----------------------------------------------------------------\n\n");
printf("                === BIENVENUE AU PARKING IVOIRE ===             \n\n");
printf("----------------------------------------------------------------\n\n");
printf("Notre parking est ouvert pour un stationnement securisé pour \nnotre clientèle... \n\n");

printf("Pour stationner, entrer '1' et pour sortir du parking, entrer '0'  \n\n");   
printf("Faites votre choix !!! \n"); 
 
scanf("%d",&choix);
}  


}