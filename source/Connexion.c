 /**
*  \file Connexion.C
*  \author OUTTARA MOCTAR - KOUADIO BONI - AGNERO NOMEL
*  \version V.21
*  \brief Programme de connexion a la base de donnees
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <mysql/mysql.h>
#include "../Header/Connexion.h"


/*
----------------------------------------------------------------------------------------
|              DECLARATION DE FONCTION DE CONNEXION A LA BASE DE DONNEE                |
----------------------------------------------------------------------------------------
*/



/**
* \fn void Connexion()
* \brief Procedure de gestion des entrees des Vehicules.
* \details La fonction effectue la connexion a la base de donnee
*/

void connexion(MYSQL *conn)
{
    
    // Declaration variables de d'acces a la base de donnee
    static char *host="localhost";
    static char *dbname="parking";
    static char *user="root";
    static char *pass="root";

    if(conn == NULL)
    {
        fprintf(stderr,"%s\n",mysql_error(conn));
        exit(0);
    }
    if(mysql_real_connect(conn, host, user, pass, dbname,0,NULL,0) == NULL)
    {
        fprintf(stderr,"%s\n",mysql_error(conn));
        mysql_close(conn);
        exit(0);
    }
    else
    {
        printf("\n");

    }
    
}
